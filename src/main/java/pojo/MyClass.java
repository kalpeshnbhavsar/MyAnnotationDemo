package pojo;

import annotation.MyAnnotation;

import java.io.*;
import java.net.URLConnection;
import java.util.Base64;

public class MyClass {

    @MyAnnotation(value = "src\\main\\resources\\file.jpg")
    private String string;

    public MyClass() {
    }

    public MyClass(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public static String getBase64(File file) {
        return Base64.getEncoder().encodeToString(new byte[(int) file.length()]);
    }

    public static String getBase64Prefix(File file) {
        try {
            return URLConnection.guessContentTypeFromName(file.getName())
                    + ";"
                    + "charset=" + new InputStreamReader(new FileInputStream(file)).getEncoding()
                    + ";base64,";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}

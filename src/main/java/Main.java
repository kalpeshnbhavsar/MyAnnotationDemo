import annotation.MyAnnotation;
import pojo.MyClass;

import java.io.File;
import java.lang.reflect.Field;

public class Main {

    public static void main(String[] args) throws Exception {

        MyClass myClass = new MyClass();

        Field field = myClass.getClass().getDeclaredField("string");
        MyAnnotation myAnnotation = field.getAnnotation(MyAnnotation.class);

        File file = new File(System.getProperty("user.dir") + File.separator + myAnnotation.value());
        System.out.println("path: " + file.getPath());
        System.out.println("prefix: " + MyClass.getBase64Prefix(file));
    }
}